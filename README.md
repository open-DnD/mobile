# DND Mobile

The purpose of the mobile version is for players who can carry their characters in a electronic way so people don't forget their sheets such people typically remember to bring their phone

# Current Features

- List Characters
- List Monsters
- List Items

# Planned Features

- Show characters stats
- Show character items
- Possibly auto dice rolls by each stat and items
- Possibly show rules, items and monsters from current Dungeons & Dragons rulebook
- Link/Sync with dungeon master version (web)

## Getting Started

For help getting started with Flutter, view online
[documentation](https://flutter.io/).
