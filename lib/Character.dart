import 'package:flutter/material.dart';
import 'helper.dart';
import 'dart:async' show Future;
import 'dart:convert';

class Character {
  String name;
  String characterClass;
  String age;
  String sex;
  String race;
  List<String> ideals;
  List<String> flaws;
  String height;
  String weight;
  String alignment;
  String background;
  List<String> spells;
  List<String> proficiency;
  List<String> equipment;

  Character(
      {this.name,
      this.characterClass,
      this.age,
      this.sex,
      this.race,
      this.ideals,
      this.flaws,
      this.height,
      this.weight,
      this.alignment,
      this.background,
      this.spells,
      this.proficiency,
      this.equipment});

  factory Character.fromJson(Map<String, dynamic> parsedJson) {
    return Character(
      name: parsedJson['Name'],
      characterClass: parsedJson['Character_Class'],
      age: parsedJson['Age'],
      sex: parsedJson['Sex'],
      race: parsedJson['Race'],
      ideals: parsedJson['Ideals'].cast<String>(),
      flaws: parsedJson['Flaws'].cast<String>(),
      height: parsedJson['Height'],
      weight: parsedJson['Weight'],
      alignment: parsedJson['Alignment'],
      background: parsedJson['Background'],
      spells: parsedJson['Spells'].cast<String>(),
      proficiency: parsedJson['Proficiency'].cast<String>(),
      equipment: parsedJson['Equipment'].cast<String>(),
    );
  }
}

class ListCharacters {
  final List<Character> characters;

  ListCharacters({
    this.characters,
  });

  factory ListCharacters.fromJson(List<dynamic> parsedJson) {
    List<Character> characters = new List<Character>();
    characters = parsedJson.map((i) => Character.fromJson(i)).toList();

    return new ListCharacters(
      characters: characters,
    );
  }
}

Future loadItems() async {
  String jsonString = await loadAsset("assets/characters.json");
  final jsonResponse = json.decode(jsonString);
  ListCharacters item = new ListCharacters.fromJson(jsonResponse);
  return item;
}

class CharactersScreen extends StatelessWidget {
  final String title;
  CharactersScreen({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<dynamic>(
        future: loadItems(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemsList(items: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ItemsList extends StatelessWidget {
  final ListCharacters items;

  ItemsList({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.characters.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text(items.characters[index].name));
      },
    );
  }
}
