import 'package:flutter/material.dart';
import 'character.dart';
import 'items.dart';
import 'spells.dart';
import 'monsters.dart';

var option = [Icons.person, Icons.android, Icons.cake, Icons.flare];

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: option.length,
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
              IconButton(icon: Icon(Icons.add), onPressed: () {}),
            ],
            bottom: TabBar(tabs: buildList(option.length)),
            title: Text('DnD App'),
          ),
          body: TabBarView(
            children: [
              CharactersScreen(),
              MonstersScreen(),
              ItemsScreen(),
              SpellsScreen()
            ],
          ),
        ),
      ),
    );
  }
}

List<Tab> buildList(int count) {
  return List<Tab>.generate(
      count, (int index) => Tab(icon: Icon(option[index])));
}
