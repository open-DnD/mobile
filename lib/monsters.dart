import 'package:flutter/material.dart';
import 'helper.dart';
import 'dart:async' show Future;
import 'dart:convert';

class Monster {
  int index;
  String name;
  Size size;
  Type type;
  Subtype subtype;
  Alignment alignment;
  int armorClass;
  int hitPoints;
  String hitDice;
  String speed;
  int strength;
  int dexterity;
  int constitution;
  int intelligence;
  int wisdom;
  int constitutionSave;
  int intelligenceSave;
  int wisdomSave;
  int history;
  int perception;
  DamageVulnerabilities damageVulnerabilities;
  String damageResistances;
  String damageImmunities;
  String conditionImmunities;
  String senses;
  String languages;
  num challengeRating;
  List<Action> specialAbilities;
  List<Action> actions;
  List<Action> legendaryActions;
  int charisma;
  int medicine;
  int religion;
  int dexteritySave;
  int charismaSave;
  int stealth;
  int persuasion;
  int insight;
  int deception;
  int arcana;
  int athletics;
  int acrobatics;
  int strengthSave;
  List<Action> reactions;
  int survival;
  int investigation;
  int nature;
  int intimidation;
  int performance;

  Monster({
    this.index,
    this.name,
    this.size,
    this.type,
    this.subtype,
    this.alignment,
    this.armorClass,
    this.hitPoints,
    this.hitDice,
    this.speed,
    this.strength,
    this.dexterity,
    this.constitution,
    this.intelligence,
    this.wisdom,
    this.constitutionSave,
    this.intelligenceSave,
    this.wisdomSave,
    this.history,
    this.perception,
    this.damageVulnerabilities,
    this.damageResistances,
    this.damageImmunities,
    this.conditionImmunities,
    this.senses,
    this.languages,
    this.challengeRating,
    this.specialAbilities,
    this.actions,
    this.legendaryActions,
    this.charisma,
    this.medicine,
    this.religion,
    this.dexteritySave,
    this.charismaSave,
    this.stealth,
    this.persuasion,
    this.insight,
    this.deception,
    this.arcana,
    this.athletics,
    this.acrobatics,
    this.strengthSave,
    this.reactions,
    this.survival,
    this.investigation,
    this.nature,
    this.intimidation,
    this.performance,
  });

  factory Monster.fromJson(Map<String, dynamic> parsedJson) {
    return Monster(
        name: parsedJson['name'] as String,
        armorClass: parsedJson['armor_class'],
        hitPoints: parsedJson['hit_points'],
        challengeRating: parsedJson['challenge_rating'] as num,
        speed: parsedJson['Speed'],
        languages: parsedJson['languages']);
  }
}

class Action {
  String name;
  String desc;
  int attackBonus;
  String damageDice;
  int damageBonus;

  Action({
    this.name,
    this.desc,
    this.attackBonus,
    this.damageDice,
    this.damageBonus,
  });
}

enum Alignment {
  LAWFUL_EVIL,
  ANY_ALIGNMENT,
  CHAOTIC_EVIL,
  CHAOTIC_GOOD,
  LAWFUL_GOOD,
  NEUTRAL,
  LAWFUL_NEUTRAL,
  UNALIGNED,
  ANY_NON_GOOD_ALIGNMENT,
  ANY_NON_LAWFUL_ALIGNMENT,
  NEUTRAL_EVIL,
  ANY_CHAOTIC_ALIGNMENT,
  NEUTRAL_GOOD,
  CHAOTIC_NEUTRAL,
  NEUTRAL_GOOD_50_OR_NEUTRAL_EVIL_50,
  ANY_EVIL_ALIGNMENT
}

enum DamageVulnerabilities {
  EMPTY,
  FIRE,
  THUNDER,
  BLUDGEONING_FIRE,
  COLD,
  BLUDGEONING,
  PIERCING_FROM_MAGIC_WEAPONS_WIELDED_BY_GOOD_CREATURES,
  RADIANT
}

enum Size { LARGE, MEDIUM, HUGE, GARGANTUAN, SMALL, TINY }

enum Subtype {
  EMPTY,
  ANY_RACE,
  DEMON,
  DEVIL,
  GOBLINOID,
  GNOME,
  SHAPECHANGER,
  ELF,
  DWARF,
  GNOLL,
  GRIMLOCK,
  HUMAN,
  KOBOLD,
  TITAN,
  LIZARDFOLK,
  MERFOLK,
  ORC,
  SAHUAGIN
}

enum Type {
  ABERRATION,
  HUMANOID,
  DRAGON,
  UNDEAD,
  ELEMENTAL,
  MONSTROSITY,
  CONSTRUCT,
  BEAST,
  PLANT,
  FIEND,
  OOZE,
  FEY,
  GIANT,
  CELESTIAL,
  SWARM_OF_TINY_BEASTS
}

class ListMonsters {
  final List<Monster> monsters;

  ListMonsters({
    this.monsters,
  });

  factory ListMonsters.fromJson(List<dynamic> parsedJson) {
    List<Monster> monsters = new List<Monster>();
    monsters = parsedJson.map((i) => Monster.fromJson(i)).toList();

    return new ListMonsters(
      monsters: monsters,
    );
  }
}

Future loadItems() async {
  String jsonString = await loadAsset("assets/monsters.json");
  final jsonResponse = json.decode(jsonString);
  ListMonsters item = new ListMonsters.fromJson(jsonResponse);
  return item;
}

class MonstersScreen extends StatelessWidget {
  final String title;
  MonstersScreen({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<dynamic>(
        future: loadItems(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemsList(items: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ItemsList extends StatelessWidget {
  final ListMonsters items;

  ItemsList({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.monsters.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text(items.monsters[index].name));
      },
    );
  }
}
