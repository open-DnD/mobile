import 'package:flutter/material.dart';
import 'helper.dart';
import 'dart:async' show Future;
import 'dart:convert';

class Item {
  int index;
  String name;
  EquipmentCategory equipmentCategory;
  WeaponCategory weaponCategory;
  WeaponRange weaponRange;
  CategoryRange categoryRange;
  Cost cost;
  Damage damage;
  Range range;
  double weight;
  List<DamageType> properties;
  Range throwRange;
  Damage the2HDamage;
  List<String> special;
  String armorCategory;
  ArmorClass armorClass;
  int strMinimum;
  bool stealthDisadvantage;
  GearCategory gearCategory;
  List<String> desc;
  List<Content> contents;
  ToolCategory toolCategory;
  VehicleCategory vehicleCategory;
  Cost speed;
  String capacity;

  Item({
    this.index,
    this.name,
    this.equipmentCategory,
    this.weaponCategory,
    this.weaponRange,
    this.categoryRange,
    this.cost,
    this.damage,
    this.range,
    this.weight,
    this.properties,
    this.throwRange,
    this.the2HDamage,
    this.special,
    this.armorCategory,
    this.armorClass,
    this.strMinimum,
    this.stealthDisadvantage,
    this.gearCategory,
    this.desc,
    this.contents,
    this.toolCategory,
    this.vehicleCategory,
    this.speed,
    this.capacity,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      index: json['index'] as num,
      name: json['name'] as String,
    );
  }
}

class ArmorClass {
  int base;
  bool dexBonus;
  int maxBonus;

  ArmorClass({
    this.base,
    this.dexBonus,
    this.maxBonus,
  });
}

enum CategoryRange {
  SIMPLE_MELEE,
  SIMPLE_RANGED,
  MARTIAL_MELEE,
  MARTIAL_RANGED
}

class Content {
  String itemUrl;
  int quantity;

  Content({
    this.itemUrl,
    this.quantity,
  });
}

class Cost {
  double quantity;
  Unit unit;

  Cost({
    this.quantity,
    this.unit,
  });
}

enum Unit { SP, GP, CP, FT_ROUND, MPH }

class Damage {
  int diceCount;
  int diceValue;
  DamageType damageType;

  Damage({
    this.diceCount,
    this.diceValue,
    this.damageType,
  });
}

class DamageType {
  String name;

  DamageType({
    this.name,
  });
}

enum EquipmentCategory {
  WEAPON,
  ARMOR,
  ADVENTURING_GEAR,
  TOOLS,
  MOUNTS_AND_VEHICLES
}

enum GearCategory {
  STANDARD_GEAR,
  AMMUNITION,
  HOLY_SYMBOL,
  ARCANE_FOCUS,
  DRUIDIC_FOCUS,
  KIT,
  EQUIPMENT_PACK
}

class Range {
  int normal;
  int long;

  Range({
    this.normal,
    this.long,
  });
}

enum ToolCategory {
  ARTISAN_S_TOOLS,
  GAMING_SETS,
  MUSICAL_INSTRUMENT,
  OTHER_TOOLS
}

enum VehicleCategory {
  MOUNTS_AND_OTHER_ANIMALS,
  TACK_HARNESS_AND_DRAWN_VEHICLES,
  WATERBORNE_VEHICLES
}

enum WeaponCategory { SIMPLE, MARTIAL }

enum WeaponRange { MELEE, RANGED }

class ItemsList {
  final List<Item> items;

  ItemsList({
    this.items,
  });

  factory ItemsList.fromJson(List<dynamic> parsedJson) {
    List<Item> items = new List<Item>();
    items = parsedJson.map((i) => Item.fromJson(i)).toList();

    return new ItemsList(
      items: items,
    );
  }
}

Future loadItems() async {
  String jsonString = await loadAsset("assets/items.json");
  final jsonResponse = json.decode(jsonString);
  ItemsList item = new ItemsList.fromJson(jsonResponse);
  return item;
}

class ItemsScreen extends StatelessWidget {
  final String title;

  ItemsScreen({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<dynamic>(
        future: loadItems(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemsListWidget(items: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ItemsListWidget extends StatelessWidget {
  final ItemsList items;

  ItemsListWidget({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.items.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text(items.items[index].name));
      },
    );
  }
}
