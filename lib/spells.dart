import 'package:flutter/material.dart';
import 'helper.dart';
import 'dart:async' show Future;
import 'dart:convert';

class Spell {
  int index;
  String name;
  List<String> desc;
  List<String> higherLevel;
  String page;
  Range range;
  List<Component> components;
  String material;
  Concentration ritual;
  String duration;
  Concentration concentration;
  CastingTime castingTime;
  int level;
  School school;
  List<School> classes;
  List<School> subclasses;

  Spell({
    this.index,
    this.name,
    this.desc,
    this.higherLevel,
    this.page,
    this.range,
    this.components,
    this.material,
    this.ritual,
    this.duration,
    this.concentration,
    this.castingTime,
    this.level,
    this.school,
    this.classes,
    this.subclasses,
  });

  factory Spell.fromJson(Map<String, dynamic> parsedJson) {
    return Spell(
      name: parsedJson['name'] as String,
    );
  }
}

enum CastingTime {
  THE_1_ACTION,
  THE_1_MINUTE,
  THE_1_HOUR,
  THE_8_HOURS,
  THE_1_BONUS_ACTION,
  THE_10_MINUTES,
  THE_1_REACTION,
  THE_24_HOURS,
  THE_12_HOURS
}

class School {
  Name name;

  School({
    this.name,
  });
}

enum Name {
  WIZARD,
  SORCERER,
  CLERIC,
  PALADIN,
  RANGER,
  BARD,
  DRUID,
  WARLOCK,
  EVOCATION,
  CONJURATION,
  ABJURATION,
  TRANSMUTATION,
  ENCHANTMENT,
  NECROMANCY,
  DIVINATION,
  ILLUSION,
  LORE,
  LAND,
  LIFE,
  DEVOTION,
  FIEND
}

enum Component { V, S, M }

enum Concentration { NO, YES }

enum Range {
  THE_90_FEET,
  THE_60_FEET,
  THE_30_FEET,
  SELF,
  THE_10_FEET,
  THE_120_FEET,
  TOUCH,
  THE_150_FEET,
  THE_1_MILE,
  THE_300_FEET,
  THE_500_FEET,
  SPECIAL,
  THE_100_FEET,
  SIGHT,
  THE_500_MILES,
  UNLIMITED,
  THE_5_FEET
}

class ListSpells {
  final List<Spell> spells;

  ListSpells({
    this.spells,
  });

  factory ListSpells.fromJson(List<dynamic> parsedJson) {
    List<Spell> spells = new List<Spell>();
    spells = parsedJson.map((i) => Spell.fromJson(i)).toList();

    return new ListSpells(
      spells: spells,
    );
  }
}

Future loadItems() async {
  String jsonString = await loadAsset("assets/spells.json");
  final jsonResponse = json.decode(jsonString);
  ListSpells item = new ListSpells.fromJson(jsonResponse);
  return item;
}

class SpellsScreen extends StatelessWidget {
  final String title;
  SpellsScreen({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<dynamic>(
        future: loadItems(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ItemsList(items: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class ItemsList extends StatelessWidget {
  final ListSpells items;

  ItemsList({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.spells.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(title: Text(items.spells[index].name));
      },
    );
  }
}
